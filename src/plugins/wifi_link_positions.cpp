/* Copyright (c) 2017-2021. The SimGrid Team. All rights reserved.          */

/* This program is free software; you can redistribute it and/or modify it
 * under the terms of the license (GNU LGPL) which comes with this package. */

#include <simgrid/Exception.hpp>

#include <simgrid/plugins/position.h>
#include <simgrid/s4u/Engine.hpp>
#include <simgrid/s4u/Host.hpp>
#include <simgrid/s4u/Link.hpp>

#include <cmath>

#include "src/surf/network_wifi.hpp"

SIMGRID_REGISTER_PLUGIN(wifi_host_position, "Wifi host position", &sg_wifi_link_positions_plugin_init);

XBT_LOG_NEW_DEFAULT_SUBCATEGORY(wifi_host_position, surf, "Logging specific to the wifi host position plugin");


namespace simgrid {
namespace plugin {

class HostPosition {
public:
  /**
   * @brief The different mobility types
   * default = static, the node only moves upon explicit setPosition call
   */
  enum class HostMobilityType { STATIC = 0 };

  double xPos_{0};
  double yPos_{0};
  double zPos_{0};

  HostPosition(double x, double y, double z)
    : xPos_(x), yPos_(y),zPos_(z) {}
  HostPosition(): HostPosition(0,0,0){}

  std::string toString() {return "("+std::to_string(xPos_)+
                            ";"+std::to_string(yPos_)+
                            ";"+std::to_string(zPos_)+")";}
};

namespace wifilinkpositions {

class XBT_PRIVATE WifiLinkPositions {
  /*x, y and z coordinates of hosts attached to the link*/
  std::map<std::string, HostPosition> hostPositions_;

  /* Coordinates of the AP */
  HostPosition positionAP_;

  /* Link to which the position plugin is attached*/
  s4u::Link* link_;

  /* distance vector to determine the SNR*/
  std::vector<double> max_distance_of_rate_;

  int chooseRate(double dist);

public:
  static xbt::Extension<simgrid::s4u::Link, WifiLinkPositions> EXTENSION_ID;

  WifiLinkPositions(s4u::Link* linkPtr, std::vector<double> snrDists);

  HostPosition get_position(s4u::Host* host);
  void set_position(s4u::Host* host, HostPosition newPosition);
  void set_SNR_dist_vector(std::vector<double> max_distance_of_rate);

  int update_SNR(s4u::Host* host);

};
xbt::Extension<s4u::Link, WifiLinkPositions> WifiLinkPositions::EXTENSION_ID;

} // namespace wifihostposition
} // namespace plugin
} // namespace simgrid

using simgrid::plugin::wifilinkpositions::WifiLinkPositions;

WifiLinkPositions::WifiLinkPositions(s4u::Link* linkPtr, std::vector<double> snrDists)
    : link_(linkPtr), max_distance_of_rate_(snrDists)
{
  xbt_assert(link_->get_sharing_policy() == simgrid::s4u::Link::SharingPolicy::WIFI,
    "WifiLinkPositions plugin can only be used on WiFi Links.");

  // verify that the snr vector is the same size as the link's host_rates (warning for now)
  auto const* wifi_link = static_cast<kernel::resource::NetworkWifiLink*>(link_->get_impl());
  if(max_distance_of_rate_.size() == wifi_link->get_host_rates_size()) {
    XBT_WARN("SNR vector gives distance boundaries between hostrates, got %u values, ideally wanted %lu",
    wifi_link->get_host_rates_size(), max_distance_of_rate_.size());
  }

  XBT_DEBUG("Successfully created WifiLinkPositions for Link %s", link_->get_cname());
}

simgrid::plugin::HostPosition WifiLinkPositions::get_position(s4u::Host* host)
{
  if(hostPositions_.find(host->get_name()) == hostPositions_.end())
    XBT_ERROR("WifiLinkPositions cannot give the position of host if it has not been provided");

  return hostPositions_.at(host->get_name());
}

void WifiLinkPositions::set_position(simgrid::s4u::Host* host, HostPosition newPosition)
{
  if(hostPositions_.find(host->get_name()) == hostPositions_.end())
    XBT_DEBUG("Adding new position for host \"%s\" %s",
      host->get_cname(),newPosition.toString().c_str());
  else {
    XBT_DEBUG("New position for host \"%s\" %s",
      host->get_cname(),newPosition.toString().c_str());
  }
  hostPositions_[host->get_name()] = newPosition;

  // once you changed the position of a node, you need to recompute its host rate
  update_SNR(host);
}

void WifiLinkPositions::set_SNR_dist_vector(std::vector<double> max_distance_of_rate)
{
  xbt_assert(max_distance_of_rate.size() >= 1, "Cannot use an empty SNR dist vector");
  max_distance_of_rate_ = max_distance_of_rate;
  XBT_DEBUG("Changed snr dist vector successfully");

  for (std::pair<std::string, HostPosition> h : hostPositions_) {
    update_SNR(simgrid::s4u::Host::by_name(h.first));
  }

}


int WifiLinkPositions::update_SNR(s4u::Host* host)
{
  xbt_assert(hostPositions_.find(host->get_name()) != hostPositions_.end(),
    "Host \"%s\" has not been given any position, cannot compute SNR level", host->get_cname());
  XBT_DEBUG("Recomputing host rate for \"%s\"", host->get_cname());

  auto* wifi_link = static_cast<kernel::resource::NetworkWifiLink*>(link_->get_impl());
  HostPosition posNode = hostPositions_.at(host->get_name());

  double dist = sqrt(pow(posNode.xPos_-positionAP_.xPos_,2)+
                    pow(posNode.yPos_-positionAP_.yPos_,2)+
                    pow(posNode.zPos_-positionAP_.zPos_,2));

  XBT_DEBUG("Distance between AP %s and \"%s\" %s = %lf",
    positionAP_.toString().c_str(), host->get_cname(), posNode.toString().c_str(), dist);

  int snr_level = chooseRate(dist);
  wifi_link->set_host_rate(host, snr_level);

  return snr_level;
}

// taken from https://stackoverflow.com/questions/8647635/elegant-way-to-find-closest-value-in-a-vector-from-above
int WifiLinkPositions::chooseRate(double dist) {
  auto const it = std::lower_bound(max_distance_of_rate_.begin(), max_distance_of_rate_.end(), dist);
  if (it == max_distance_of_rate_.end()) { return -1; }

  int level = std::distance(max_distance_of_rate_.begin(), it);
  XBT_DEBUG("Computed SNR level: %d", level);
  return level;
}

void sg_wifi_link_positions_plugin_init()
{
  if (WifiLinkPositions::EXTENSION_ID.valid())
    return;

  XBT_INFO("Activating the wifi_link_position plugin.");
  WifiLinkPositions::EXTENSION_ID = simgrid::s4u::Link::extension_create<WifiLinkPositions>();

  simgrid::s4u::Link::on_creation.connect([](simgrid::s4u::Link& link) {
  // verify the link is using a WiFi policy
  if (link.get_sharing_policy() == simgrid::s4u::Link::SharingPolicy::WIFI) {
    XBT_DEBUG("Wifi Link: %s, initialization of wifi position plugin", link.get_cname());
    std::vector<double> v{0};
    auto* plugin = new WifiLinkPositions(&link, v);
    link.extension_set(plugin);
  }
  });

  simgrid::s4u::Link::on_destruction.connect([](simgrid::s4u::Link const& link) {
    // outputs if needed
    XBT_INFO("wifi position destoying link");
  });
}

static void ensure_plugin_inited()
{
  if (not WifiLinkPositions::EXTENSION_ID.valid())
    throw simgrid::xbt::InitializationError("The plugin is not active. Please call sg_wifi_link_positions_plugin_init() "
                                            "before calling any function related to that plugin.");
}

void sg_wifi_link_positions_set_snr_dist_vect(const_sg_link_t link, std::vector<double> vect)
{
  ensure_plugin_inited();
  xbt_assert(link->get_sharing_policy() == simgrid::s4u::Link::SharingPolicy::WIFI,
    "You can't call this function on a non WiFi link");
  link->extension<WifiLinkPositions>()->set_SNR_dist_vector(vect);
}

void sg_wifi_link_positions_set_host_position(const_sg_link_t link, sg_host_t host, double x, double y, double z)
{
  ensure_plugin_inited();
  xbt_assert(link->get_sharing_policy() == simgrid::s4u::Link::SharingPolicy::WIFI,
    "You can't call this function on a non WiFi link");

  simgrid::plugin::HostPosition p = simgrid::plugin::HostPosition(x, y, z);
  link->extension<WifiLinkPositions>()->set_position(host, p);

}
