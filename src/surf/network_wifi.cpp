/* Copyright (c) 2019-2021. The SimGrid Team. All rights reserved.          */

/* This program is free software; you can redistribute it and/or modify it
 * under the terms of the license (GNU LGPL) which comes with this package. */

#include "network_wifi.hpp"
#include "simgrid/s4u/Host.hpp"
#include "src/surf/surf_interface.hpp"
#include "surf/surf.hpp"
#include <simgrid/s4u.hpp>

XBT_LOG_EXTERNAL_DEFAULT_CATEGORY(res_network);

namespace simgrid {
namespace kernel {
namespace resource {

/************
 * Resource *
 ************/

NetworkWifiLink::NetworkWifiLink(const std::string& name, const std::vector<double>& bandwidths, lmm::System* system)
    : LinkImpl(name), nb_active_flux_(0)
{
  this->set_constraint(system->constraint_new(this, 1));
  for (auto bandwidth : bandwidths)
    bandwidths_.push_back({bandwidth, 1.0, nullptr});

  /**
   * Version using a manual counter on communication events
   * Should I switch to the nonlinear feature?
   */
  s4u::Link::on_communicate.connect(&update_bw_comm_start);
  s4u::Link::on_communication_state_change.connect(&update_bw_comm_end);

}

double NetworkWifiLink::wifi_link_dynamic_sharing(NetworkWifiLink* link, double capacity, int n)
{
  XBT_INFO("!!!!!!!!!!!!!!!!!!!! Nonlinear Callback cap: %lf n: %d\n", capacity, n);
  double ratio = link->get_max_ratio();
  XBT_INFO("New ratio value: %lf of link capacity on link %s", ratio, link->get_name().c_str());
  return ratio;
}

double NetworkWifiLink::get_max_ratio()
{
  double new_peak = -1;
  if(nb_active_flux_ > nbF_lim_){
    new_peak = (nb_active_flux_-nbF_lim_) * co_acc_ + x0_;
    XBT_INFO("peak=(%d-%d)*%lf+%lf=%lf",nb_active_flux_,nbF_lim_,co_acc_,x0_,new_peak);
  }else{
    new_peak = x0_;
    XBT_INFO("peak=%lf",x0_);
  }
  // should be the new maximum bandwidth ratio (comparison between max throughput without concurrency and with it)
  double propCap = new_peak/x0_;

  return propCap;
}

void NetworkWifiLink::update_bw_comm_start(simgrid::kernel::resource::NetworkAction& action)
{
  auto const* actionWifi = dynamic_cast<const simgrid::kernel::resource::NetworkWifiAction*>(&action);
  if (actionWifi == nullptr)
    return;

  auto* link_src = actionWifi->get_src_link();
  auto* link_dst = actionWifi->get_dst_link();
  if(link_src != nullptr) {
    link_src->inc_active_flux();
  }
  if(link_dst != nullptr) {
    link_dst->inc_active_flux();
  }
}

void NetworkWifiLink::update_bw_comm_end(simgrid::kernel::resource::NetworkAction& action, simgrid::kernel::resource::Action::State state)
{
  if(action.get_state() != kernel::resource::Action::State::FINISHED)
    return;

  auto const* actionWifi = dynamic_cast<const simgrid::kernel::resource::NetworkWifiAction*>(&action);
  if (actionWifi == nullptr)
    return;

  auto* link_src = actionWifi->get_src_link();
  auto* link_dst = actionWifi->get_dst_link();
  if(link_src != nullptr) {
    link_src->dec_active_flux();
  }
  if(link_dst != nullptr) {
    link_dst->dec_active_flux();
  }
}

void NetworkWifiLink::inc_active_flux() {
  xbt_assert(nb_active_flux_>=0, "Negative nb_active_flux should not exist");
  nb_active_flux_++;
  if(use_decay_model_)
    refresh_decay_bandwidths();
}

void NetworkWifiLink::dec_active_flux() {
  xbt_assert(nb_active_flux_>0, "Negative nb_active_flux should not exist");
  nb_active_flux_--;
  if(use_decay_model_)
    refresh_decay_bandwidths();
}

void NetworkWifiLink::set_host_rate(const s4u::Host* host, int rate_level)
{
  //XBT_INFO("%s %d", host->get_name().c_str(), rate_level);
  auto insert_done = host_rates_.insert(std::make_pair(host->get_name(), rate_level));
  if (not insert_done.second)
    insert_done.first->second = rate_level;

  // Each time we add a host, we refresh the decay model
  refresh_decay_bandwidths();
}

double NetworkWifiLink::get_host_rate(const s4u::Host* host) const
{
  auto host_rates_it = host_rates_.find(host->get_name());

  if (host_rates_it == host_rates_.end())
    return -1;

  int rate_id = host_rates_it->second;
  xbt_assert(rate_id >= 0,
             "Negative host wifi rate levels are invalid but host '%s' uses %d as a rate level on link '%s'",
             host->get_cname(), rate_id, this->get_cname());
  xbt_assert(rate_id < (int)bandwidths_.size(),
             "Link '%s' only has %zu wifi rate levels, so the provided level %d is invalid for host '%s'.",
             this->get_cname(), bandwidths_.size(), rate_id, host->get_cname());

  Metric rate = use_decay_model_ ? decay_bandwidths_[rate_id] : bandwidths_[rate_id];
  return rate.peak * rate.scale;
}

unsigned int NetworkWifiLink::get_host_rates_size() const
{
  return host_rates_.size();
}

s4u::Link::SharingPolicy NetworkWifiLink::get_sharing_policy() const
{
  return s4u::Link::SharingPolicy::WIFI;
}

int NetworkWifiLink::get_host_count() const
{
  return static_cast<int>(host_rates_.size());
}

void NetworkWifiLink::refresh_decay_bandwidths(){

    std::vector<Metric> new_bandwidths;
    for (auto const& bandwidth : bandwidths_) {
      // Instantiate decay model relatively to the actual bandwidth
     XBT_INFO("refresh_bandwidth called, nb flux=%d",nb_active_flux_);

      //double new_peak = x0_;
      new_bandwidths.push_back({bandwidth.peak, 1.0, nullptr});
    }
    decay_bandwidths_=new_bandwidths;
}

bool NetworkWifiLink::toggle_decay_model(){
  use_decay_model_ = not use_decay_model_;
  this->set_sharing_policy(simgrid::s4u::Link::SharingPolicy::WIFI,
    std::bind(&wifi_link_dynamic_sharing, this, std::placeholders::_1, std::placeholders::_2));
  return use_decay_model_;
}

void NetworkWifiLink::set_latency(double value)
{
  xbt_assert(value == 0, "Latency cannot be set for WiFi Links.");
}
} // namespace resource
} // namespace kernel
} // namespace simgrid
