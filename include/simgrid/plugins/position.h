/* Copyright (c) 2016-2021. The SimGrid Team. All rights reserved.          */

/* This program is free software; you can redistribute it and/or modify it
 * under the terms of the license (GNU LGPL) which comes with this package. */

#ifndef SIMGRID_PLUGINS_WIFI_HOST_POSITION_H_
#define SIMGRID_PLUGINS_WIFI_HOST_POSITION_H_

#include <simgrid/config.h>
#include <simgrid/forward.h>
#include <xbt/base.h>

SG_BEGIN_DECL
/** @ingroup plugin_wifi_host_position
 *  @brief Initialize the wifi link positions plugin */
XBT_PUBLIC void sg_wifi_link_positions_plugin_init();
XBT_PUBLIC void sg_wifi_link_positions_set_snr_dist_vect(const_sg_link_t link, std::vector<double> vect);
XBT_PUBLIC void sg_wifi_link_positions_set_host_position(const_sg_link_t link, sg_host_t host, double x, double y, double z);

SG_END_DECL

#endif
