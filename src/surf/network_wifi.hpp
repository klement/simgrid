/* Copyright (c) 2019-2021. The SimGrid Team. All rights reserved.          */

/* This program is free software; you can redistribute it and/or modify it
 * under the terms of the license (GNU LGPL) which comes with this package. */

#ifndef SURF_NETWORK_WIFI_HPP_
#define SURF_NETWORK_WIFI_HPP_

#include <xbt/base.h>

#include "network_cm02.hpp"
#include "xbt/string.hpp"

/***********
 * Classes *
 ***********/

namespace simgrid {
namespace kernel {
namespace resource {

class XBT_PRIVATE NetworkWifiAction;

class NetworkWifiLink : public LinkImpl {
  /** @brief Hold every rates association between host and links (host name, rates id) */
  std::map<xbt::string, int> host_rates_;

  /** @brief A link can have several bandwidths attached to it (mostly use by wifi model) */
  std::vector<Metric> bandwidths_;

  /** @brief Should we use the decay model ? */
  bool use_decay_model_=false;

  // values for an improved model instead of the decay
  int nb_active_flux_;
  const double x0_ = 5529372;
  const double co_acc_ = -6178;
  const int nbF_lim_ = 20;

  /** @brief The bandwidth to use for each SNR level, corrected with the decay rescale mechanism */
  std::vector<Metric> decay_bandwidths_;

public:
  NetworkWifiLink(const std::string& name, const std::vector<double>& bandwidths, lmm::System* system);

  void set_host_rate(const s4u::Host* host, int rate_level);
  /** @brief Get the AP rate associated to the host (or -1 if not associated to the AP) */
  double get_host_rate(const s4u::Host* host) const;

  /** @brief The sharing policy */
  s4u::Link::SharingPolicy get_sharing_policy() const override;

  void apply_event(kernel::profile::Event*, double) override { THROW_UNIMPLEMENTED; }
  void set_bandwidth(double) override { THROW_UNIMPLEMENTED; }
  void set_latency(double) override;
  void refresh_decay_bandwidths();
  bool toggle_decay_model();
  int get_host_count() const;
  unsigned int get_host_rates_size() const;

  // callbacks
  static void update_bw_comm_start(simgrid::kernel::resource::NetworkAction& action);
  static void update_bw_comm_end(simgrid::kernel::resource::NetworkAction& action, simgrid::kernel::resource::Action::State state);
  void inc_active_flux();
  void dec_active_flux();
  static double wifi_link_dynamic_sharing(NetworkWifiLink* link, double capacity, int n);
  double get_max_ratio();

};

class NetworkWifiAction : public NetworkCm02Action {
  NetworkWifiLink* src_wifi_link_;
  NetworkWifiLink* dst_wifi_link_;

public:
  NetworkWifiAction() = delete;
  NetworkWifiAction(Model* model, s4u::Host& src, s4u::Host& dst, double cost, bool failed,
                    NetworkWifiLink* src_wifi_link, NetworkWifiLink* dst_wifi_link)
      : NetworkCm02Action(model, src, dst, cost, failed)
      , src_wifi_link_(src_wifi_link)
      , dst_wifi_link_(dst_wifi_link)
    {}

  NetworkWifiLink* get_src_link() const { return src_wifi_link_; }
  NetworkWifiLink* get_dst_link() const { return dst_wifi_link_; }
};

} // namespace resource
} // namespace kernel
} // namespace simgrid
#endif /* SURF_NETWORK_WIFI_HPP_ */
